#!/usr/bin/env python2
from scapy.all import *
from ethercat_packet import EtherCATHeader, EtherCATDatagram

IFACE = "eno1"
DST_MAC = "ff:ff:ff:ff:ff:ff"
SRC_MAC = "00:00:00:00:00:00"
p = Ether(dst=DST_MAC, src=SRC_MAC)/EtherCATHeader()/\
    EtherCATDatagram(Cmd="BRD")
o = srp(p, iface=IFACE)
print(" === SENT ===")
o[0][0][0].show()
print(" === RECEIVED ===")
o[0][0][1].show()
