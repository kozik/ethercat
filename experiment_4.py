#!/usr/bin/env python2
from ethercat_device import EtherCATDevice

IFACE = "eno1"
device = EtherCATDevice(IFACE)
device.dump_eeprom()
