#!/usr/bin/env python2
from time import sleep
from ethercat_device import EtherCATDevice


class StepMotor:
    def __init__(self, device):
        self.device = device
        self.ap = 1
        self.am = 2
        self.bp = 4
        self.bm = 8
        self.steps = [self.ap + self.bp,
                      self.ap + self.bm,
                      self.am + self.bm,
                      self.am + self.bp]
        self.step = 0

    def next(self, dir):
        self.step += 1 if dir > 0 else -1
        self.step %= len(self.steps)
        self.device.write_output(self.steps[self.step])
        print("{}: {}".format(self.step, self.steps[self.step]))


if __name__ == "__main__":
    IFACE = "eno1"
    device = EtherCATDevice(IFACE)
    device.init()
    sm = StepMotor(device)

    while 1:
        sm.next(1)
        sleep(0.1)
