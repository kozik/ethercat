#!/usr/bin/env python2
from scapy.all import *
from time import sleep
from ethercat_packet import EtherCATHeader, EtherCATDatagram

AL_CONTROL = 0x0120
INIT = "\x01\x00"
PREOP = "\x02\x00"
SAFEOP = "\x04\x00"
OP = "\x08\x00"
BOOT = "\x03\x00"

IFACE = "eno1"
DST_MAC = "ff:ff:ff:ff:ff:ff"
SRC_MAC = "00:00:00:00:00:00"
ADDR = 0

p = Ether(dst=DST_MAC, src=SRC_MAC)/EtherCATHeader()/\
    EtherCATDatagram(Cmd="APWR", Address=ADDR,
        Offset=AL_CONTROL, data=Raw("\x00\x00"))

for cmd in [BOOT, INIT, PREOP, SAFEOP, OP]:
    p[EtherCATDatagram].data = cmd
    o = srp(p, iface=IFACE)
    sleep(20)
