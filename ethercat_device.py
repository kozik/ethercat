#!/usr/bin/env python2
from scapy.all import *
from ethercat_packet import EtherCATHeader, EtherCATDatagram

# EtherCAT registers
TYPE = 0x0000

AL_CONTROL = 0x0120
AL_CONTROL_SIZE = 2
AL_CONTROL_INIT = "\x01\x00"
AL_CONTROL_PREOP = "\x02\x00"
AL_CONTROL_SAFEOP = "\x04\x00"
AL_CONTROL_OP = "\x08\x00"
AL_STATUS = 0x0130
AL_STATUS_SIZE = 2

PDI_CONTROL = 0x0140
ESC_Configuration = 0x0141
PDI_CONFIGURATION = 0x0150
PDI_CONFIGURATION_EXT = 0x0152
PDI_CONFIGURATION_EXT_SIZE = 2

WATCHDOG_DISABLED = "\x00\x00"
WATCHDOG_TIME_PDI = 0x0410
WATCHDOG_TIME_PDI_SIZE = 2
WATCHDOG_TIME_PROCESS_DATA = 0x0420
WATCHDOG_TIME_PROCESS_DATA_SIZE = 2
WATCHDOG_STATUS_PROCESS_DATA = 0x0440
WATCHDOG_STATUS_PROCESS_DATA_SIZE = 2

EEPROM_configuration = 0x500
EEPROM_PDI_access_state = 0x501
EEPROM_control = 0x0502
EEPROM_control_size = 2
EEPROM_control_busy = 1 << 15
EEPROM_control_read_bytes = 1 << 6
EEPROM_control_read = "\x00\x01"
EEPROM_address = 0x0504
EEPROM_address_size = 4
EEPROM_data = 0x0508

DIGITAL_IO_OUT = 0x0f00
DIGITAL_IO_OUT_SIZE = 4

DIGITAL_IO_IN = 0x1000
DIGITAL_IO_IN_SIZE = 4


class EtherCATDevice(object):
    def __init__(self, iface, addr=0, dst_mac="ff:ff:ff:ff:ff:ff",
                 src_mac="01:01:01:01:01:01", verbose=0):
        self.iface = iface
        self.addr = addr
        self.src_mac = src_mac
        self.dst_mac = dst_mac
        self.verbose = verbose
        self.p = Ether(dst=self.dst_mac, src=self.src_mac) / EtherCATHeader()

    @staticmethod
    def reg_to_int(data):
        value = 0
        str_data = str(data)
        for d in str_data[::-1]:
            value *= 256
            value += ord(d)
        return value

    @staticmethod
    def int_to_reg(data, len):
        out = ""
        for l in range(len):
            out += chr(data & 255)
            data >>= 8
        return out

    @staticmethod
    def hex(data):
        print("0x" + " 0x".join("{:02x}".format(ord(c)) for c in str(data)))

    def sr(self, p):
        o = srp(p, iface=self.iface, verbose=self.verbose)
        return o[0][0][1]

    def reg_read(self, reg_addr, len=1):
        p = self.p / EtherCATDatagram(Cmd="APRD", Address=self.addr,
                                      Offset=reg_addr, data=Raw("\x00"*len))
        o = self.sr(p)
        data = o[EtherCATDatagram].data[0]
        return data

    def reg_wrt(self, reg_addr, data):
        p = self.p / EtherCATDatagram(Cmd="APWR", Address=self.addr,
                                      Offset=reg_addr, data=Raw(data))
        o = self.sr(p)
        return o

    def eeprom_read(self, eeprom_addr):
        d = self.reg_read(EEPROM_control, EEPROM_control_size)
        v = self.reg_to_int(d)
        if v & EEPROM_control_busy:
            raise MemoryError("EEPROM busy")
        read_size = 8 if v & EEPROM_control_read_bytes else 4
        self.reg_wrt(EEPROM_address, self.int_to_reg(
            eeprom_addr, EEPROM_address_size))
        self.reg_wrt(EEPROM_control, EEPROM_control_read)
        v = EEPROM_control_busy
        while v & EEPROM_control_busy:
            d = self.reg_read(EEPROM_control, EEPROM_control_size)
            v = self.reg_to_int(d)
        d = self.reg_read(EEPROM_data, read_size)
        return d

    def dump_eeprom(self):
        d = self.reg_read(EEPROM_control, EEPROM_control_size)
        v = self.reg_to_int(d)
        read_size = 4 if v & EEPROM_control_read_bytes else 2
        for a in range(0, 64, 8):
            data = ""
            for a1 in range(a, a+7, read_size):
                data += str(self.eeprom_read(a1))
            self.hex(data)

    def set_operational_state(self):
        self.reg_wrt(AL_CONTROL, AL_CONTROL_PREOP)
        self.reg_wrt(AL_CONTROL, AL_CONTROL_SAFEOP)
        self.reg_wrt(AL_CONTROL, AL_CONTROL_OP)

    def reset_process_data_watchdog(self):
        self.reg_wrt(WATCHDOG_TIME_PROCESS_DATA, WATCHDOG_DISABLED)

    def init(self):
        self.set_operational_state()
        self.reset_process_data_watchdog()

    def read_input(self):
        d = self.reg_read(DIGITAL_IO_IN)
        return self.reg_to_int(d)

    def write_output(self, d):
        self.reg_wrt(DIGITAL_IO_OUT, self.int_to_reg(d, 1))
