#!/usr/bin/env python2
from time import sleep
from ethercat_device import EtherCATDevice

IFACE = "eno1"
device = EtherCATDevice(IFACE)
device.init()

for i in range(50):
    for j in range(8):
        device.write_output(1 << j)
        sleep(0.1)
