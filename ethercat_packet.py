#!/usr/bin/env python2
from scapy.all import *


class EtherCATHeader(Packet):
    EtherType = 0x88a4
    name = "EtherCATHeader"
    fields_desc = [ShortField("LenResType", 0x0010)]

    def guess_payload_class(self, payload):
        return EtherCATDatagram


bind_layers(Ether, EtherCATHeader, type=EtherCATHeader.EtherType)
bind_layers(UDP, EtherCATHeader, dport=EtherCATHeader.EtherType)

M = 1 << 15
C = 1 << 14
LEN_MAX = (1 << 11) - 1


class EtherCATDatagram(Packet):
    commands = {0: "NOP", 1: "APRD", 2: "APWR", 3: "APRW", 4: "FPRD",
                5: "FPWR", 6: "FPRW", 7: "BRD", 8: "BWR", 9: "BRW",
                10: "LRD", 11: "LWR", 12: "LRW", 13: "ARMW", 14: "FRMW"}
    name = "EtherCATDatagram"
    fields_desc = [ByteEnumField("Cmd", 0, commands),
                   ByteField("Idx", 0),
                   XLEShortField("Address", 0),
                   XLEShortField("Offset", 0),
                   FieldLenField("LenResCM", None, length_of="data",
                                 adjust=lambda pkt, x:x | M
                                 if isinstance(pkt.payload, EtherCATDatagram)
                                 else x, fmt="<H"),
                   ShortField("IRQ", 0),
                   PacketListField("data", None, cls=Raw,
                                   length_from=
                                   lambda pkt:(pkt.LenResCM & LEN_MAX)),
                   LEShortField("WC", 0)]

    def guess_payload_class(self, payload):
        if self.LenResCM & M == M:
            return EtherCATDatagram
        else:
            return
