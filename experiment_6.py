#!/usr/bin/env python2
from time import sleep
from ethercat_device import EtherCATDevice

IFACE = "eno1"
device = EtherCATDevice(IFACE)
device.init()

for i in range(50):
        s = device.read_input()
        print("{:0>8b}".format(s))
        sleep(1)
